package com.aswdc.pcturedairy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    Button btn, btnSeeMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String msg = intent.getStringExtra("txt");
        final TextView display = findViewById(R.id.TVDisplaySecond);

        display.setText(msg);

        btn = findViewById(R.id.btnActBack);
        btnSeeMore = findViewById(R.id.btnActMore);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = getIntent();
                String webMsg = intent1.getStringExtra("web");

                if(webMsg.contentEquals("igWeb")) {
                    Intent intentWeb = new Intent(Intent.ACTION_VIEW , Uri.parse("https://minecraft.gamepedia.com/Iron_Golem"));
                    startActivity(intentWeb);
                }
                else if(webMsg.contentEquals("wolfWeb")){
                    Intent intentWeb = new Intent(Intent.ACTION_VIEW , Uri.parse("https://minecraft.gamepedia.com/Wolf"));
                    startActivity(intentWeb);
                }
                else if(webMsg.contentEquals("foxWeb")){
                    Intent intentWeb = new Intent(Intent.ACTION_VIEW , Uri.parse("https://minecraft.gamepedia.com/Fox"));
                    startActivity(intentWeb);
                }
                else if(webMsg.contentEquals("piglinWeb")){
                    Intent intentWeb = new Intent(Intent.ACTION_VIEW , Uri.parse("https://minecraft.gamepedia.com/piglin"));
                    startActivity(intentWeb);
                }
                else if(webMsg.contentEquals("witherWeb")){
                    Intent intentWeb = new Intent(Intent.ACTION_VIEW , Uri.parse("https://minecraft.gamepedia.com/wither"));
                    startActivity(intentWeb);
                }
                else if(webMsg.contentEquals("creeperWeb")){
                    Intent intentWeb = new Intent(Intent.ACTION_VIEW , Uri.parse("https://minecraft.gamepedia.com/Crepeer"));
                    startActivity(intentWeb);
                }
            }
        });
    }
}