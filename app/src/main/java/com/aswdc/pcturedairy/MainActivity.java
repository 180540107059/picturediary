package com.aswdc.pcturedairy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ImageView ironGolem , creeper , wither , fox , wolf , piglin ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ironGolem = findViewById(R.id.ivIronGolem);
        creeper = findViewById(R.id.ivCreeper);
        wither = findViewById(R.id.ivWither);
        fox = findViewById(R.id.ivFox);
        wolf = findViewById(R.id.ivWolf);
        piglin = findViewById(R.id.ivPiglin);

        ironGolem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ironGolemTxt = getString(R.string.iron_golem_txt);

                Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                intent.putExtra("txt" , ironGolemTxt);
                intent.putExtra("web" , "igWeb");
                startActivity(intent);
            }
        });

        creeper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String creeperTxt = getString(R.string.creeper_txt);

                Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                intent.putExtra("txt" , creeperTxt);
                intent.putExtra("web" , "creeperWeb");
                startActivity(intent);
            }
        });

        fox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String foxTxt = getString(R.string.lbl_picture_text);

                Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                intent.putExtra("txt" , foxTxt);
                intent.putExtra("web" , "foxWeb");
                startActivity(intent);
            }
        });

        wolf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String wolfTxt = getString(R.string.wolf_txt);

                Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                intent.putExtra("txt" , wolfTxt);
                intent.putExtra("web" , "wolfWeb");
                startActivity(intent);

            }
        });

        piglin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String piglinTxt = "Piglins often found in nether realm. They are not hostile mob unless you attack them which i'd suggest" +
                        "dont cause they attack on you with the group and there is nothing you can do about it";

                Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                intent.putExtra("txt" , piglinTxt);
                intent.putExtra("web" , "piglinWeb");
                startActivity(intent);
            }
        });

        wither.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String witherTxt = "Wither are these explosive boss of minecraft that you create yourself from soul sand and wither skull" +
                        "and just after you create them they become this big ass problem for you just like your other problem get it haha";

                Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                intent.putExtra("txt" , witherTxt);
                intent.putExtra("web" , "witherWeb");
                startActivity(intent);
            }
        });
    }
}